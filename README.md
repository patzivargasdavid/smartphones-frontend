# Project CRUD WHIT APIS

## smartphones-frontend

### Video Demostracion [](https://youtu.be/By8_IxBnoTw) https://youtu.be/By8_IxBnoTw

Este repositorio contiene el código fuente del frontend para la aplicación de gestión de smartphones. Utiliza tecnologías como Node.js y Next.js para desarrollar la interfaz de usuario y realizar operaciones CRUD en la lista de smartphones. Se utiliza el framework CSS MUI (Material-UI) para un diseño atractivo.

Hecho en JSX
Framework Next v13

## Tecnologias

- Next vs13 - Framework del proyecto
- Jsx - Jsx lenguaje de programacion

## Instalación

- git clone https://gitlab.com/patzivargasdavid/smartphones-frontend.git
- cd smartphones-frontend
- npm install
- npm run dev

## Licencia
Para proyectos de código abierto, diga cómo se licencia.
