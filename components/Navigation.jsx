"use client"
import Link from "next/link"
import { AppBar, Toolbar, Typography, Button } from '@mui/material';

function Navigation() {
    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        My App
                    </Typography>
                    <Button color="inherit" component={Link} href="/">Home</Button>
                </Toolbar>
            </AppBar>
        </>
    )
}

export default Navigation