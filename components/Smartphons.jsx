"use client"
// import fetch from 'isomorphic-unfetch';
import { useRouter } from "next/navigation"
import React, { useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import { List, ListItem, ListItemText, Button, Typography } from '@mui/material';

function Smartphones({ smartphones }) {
    const showNotification = (message, type) => {
        if (type === 'success') {
            alert(message);
        } else if (type === 'error') {
            alert(message);
        }
    };

    const [updatedSmartphones, setUpdatedSmartphones] = useState(smartphones);
    async function eliminarDato(id) {
        const apiUrl = `http://localhost:8000/api/smartphones/${id}?timestamp=${Date.now()}`;
        const response = await fetch(apiUrl, {
            method: 'DELETE',
        });
        if (response.ok) {
            const apiUrl = `http://localhost:8000/api/smartphones?timestamp=${Date.now()}`;
            const response = await fetch(apiUrl);
            const data = await response.json();
            setUpdatedSmartphones(data.filter(pone => pone.id !== id));
            showNotification('Eliminado exitosamente', 'success');
        } else {
            showNotification('No se elimino', 'error');
        }
    }

    const router = useRouter()
    return (
        <>
            <h1>Smartphones</h1>
            <List>
                {updatedSmartphones.map((smartphon) => (
                    <ListItem key={smartphon.id}>
                        <ListItemText
                            primary={smartphon.nombre}
                            secondary={`Modelo: ${smartphon.modelo}, PrecioVenta: ${smartphon.precioVenta}`}
                        />
                        <Button variant="outlined" color="primary"
                            onClick={() => {
                                router.push(`/edit/${smartphon.id}`)
                            }}>
                            Modificar
                        </Button>
                        <Button variant="outlined" color="error" onClick={() => eliminarDato(smartphon.id)}>
                            Eliminar
                        </Button>
                        <Button
                            onClick={() => {
                                router.push(`/detalles/${smartphon.id}`)
                            }}
                        ><SearchIcon /></Button>
                    </ListItem>
                ))}
            </List>
            <Button
                onClick={() => {
                    router.push(`/agregar`)
                }}
            >Agregar nuevo</Button>
        </>
    )
}

export default Smartphones