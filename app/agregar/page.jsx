"use client"
import React, { useState } from 'react';
import { TextField, Button } from '@mui/material';

async function guardarDatos(nuevoDato) {
    const apiUrl = `http://localhost:8000/api/smartphones/?timestamp=${Date.now()}`;
    // console.log('nuevoDato')
    // console.log(nuevoDato)
    const response = await fetch(apiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(nuevoDato),
    });
    const data = await response.json();

    return data;
}

function AgregarDato() {
    const [nombre, setNombre] = useState('');
    const [modelo, setModelo] = useState('');
    const [precioReferencial, setPrecioReferencial] = useState('');
    const [precioVenta, setPrecioVenta] = useState('');
    const [añoModelo, setAñoModelo] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const nuevoDato = {
            nombre,
            modelo,
            precioReferencial,
            precioVenta,
            añoModelo
        };
        try {
            const data = await guardarDatos(nuevoDato);
            alert('Se agrego exitosamente');
        } catch (error) {
            alert('Error al guardar los datos:');
        }
        setNombre('');
        setModelo('');
        setPrecioReferencial('');
        setPrecioVenta('');
        setAñoModelo('');
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>Agregar nuevo dato</h2>
            <div>
                <label>Nombre:</label>
                <input type="text" value={nombre} onChange={(e) => setNombre(e.target.value)} />
            </div>
            <div>
                <label>Modelo:</label>
                <input type="text" value={modelo} onChange={(e) => setModelo(e.target.value)} />
            </div>
            <div>
                <label>Precio de referencial:</label>
                <input type="number" value={precioReferencial} onChange={(e) => setPrecioReferencial(e.target.value)} />
            </div>
            <div>
                <label>Precio Venta:</label>
                <input type="number" value={precioVenta} onChange={(e) => setPrecioVenta(e.target.value)} />
            </div>
            <div>
                <label>Año Modelo:</label>
                <input type="number" value={añoModelo} onChange={(e) => setAñoModelo(e.target.value)} />
            </div>
            <button type="submit">Agregar</button>
        </form>
    );
}

export default AgregarDato;
// const SmartphoneForm = ({ onSubmit, initialValues }) => {
//     const [name, setName] = useState(initialValues?.name || '');
//     const [model, setModel] = useState(initialValues?.model || '');
//     const [referencePrice, setReferencePrice] = useState(
//         initialValues?.referencePrice || ''
//     );
//     const [year, setYear] = useState(initialValues?.year || '');

//     const handleSubmit = (e) => {
//         e.preventDefault();
//         const smartphoneData = {
//             name,
//             model,
//             referencePrice,
//             year,
//         };
//         onSubmit(smartphoneData);
//     };

//     return (
//         <form onSubmit={handleSubmit}>
//             <TextField
//                 label="Name"
//                 value={name}
//                 onChange={(e) => setName(e.target.value)}
//             />
//             <TextField
//                 label="Model"
//                 value={model}
//                 onChange={(e) => setModel(e.target.value)}
//             />
//             <TextField
//                 label="Reference Price"
//                 value={referencePrice}
//                 onChange={(e) => setReferencePrice(e.target.value)}
//             />
//             <TextField
//                 label="Year"
//                 value={year}
//                 onChange={(e) => setYear(e.target.value)}
//             />
//             <Button type="submit">Submit</Button>
//         </form>
//     );
// };

// export default SmartphoneForm;
