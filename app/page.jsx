// import SmartphoneList from "@/components/SmartphoneList";
import Smartphones from "@/components/Smartphons";
// import { obtenerDatos, guardarDato, actualizarDato, eliminarDato } from '../components/CRUD';

// Leer datos (Operación GET)
export async function obtenerDatos() {
  const apiUrl = `http://localhost:8000/api/smartphones?timestamp=${Date.now()}`;
  const response = await fetch(apiUrl);
  const data = await response.json();
  return data;
}
async function IndexPage() {
  const smartphones = await obtenerDatos();
  return (
    <div>
      <div>
        <Smartphones
          smartphones={smartphones}
        />
      </div>
    </div>
  )
}

export default IndexPage