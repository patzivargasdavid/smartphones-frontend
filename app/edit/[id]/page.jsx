"use client"
import React, { useState, useEffect } from 'react';
import { TextField, Button } from '@mui/material';
import { useHistory } from 'react-router-dom';

async function obtenerDato(id) {
    const apiUrl = `http://localhost:8000/api/smartphones/${id}`;
    const response = await fetch(apiUrl);
    const data = await response.json();

    return data;
}

async function actualizarDato(id, updatedData) {
    const apiUrl = `http://localhost:8000/api/smartphones/${id}`;
    const response = await fetch(apiUrl, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedData),
    });
    const data = await response.json();

    return data;
}

function EditarDato({ match }) {
    const [nombre, setNombre] = useState('');
    const [modelo, setModelo] = useState('');
    const [precioReferencial, setPrecioReferencial] = useState('');
    const [precioVenta, setPrecioVenta] = useState('');
    const [añoModelo, setAñoModelo] = useState('');
    const history = useHistory();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await obtenerDato(match.params.id);
                console.log(data)
                setNombre(data.nombre);
                setModelo(data.modelo);
                setPrecioReferencial(data.precioReferencial);
                setPrecioVenta(data.precioVenta);
                setAñoModelo(data.añoModelo);
            } catch (error) {
                console.error('Error al obtener el dato:', error);
            }
        };

        fetchData();
    }, [match.params.id]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const updatedDato = {
            nombre,
            modelo,
            precioReferencial,
            precioVenta,
            añoModelo
        };

        try {
            const data = await actualizarDato(match.params.id, updatedDato);
            alert('Dato modificado exitosamente');
        } catch (error) {
            alert('Error al modificar el dato');
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <h2>Editar dato</h2>
            <div>
                <label>Nombre:</label>
                <input type="text" value={nombre} onChange={(e) => setNombre(e.target.value)} />
            </div>
            <div>
                <label>Modelo:</label>
                <input type="text" value={modelo} onChange={(e) => setModelo(e.target.value)} />
            </div>
            <div>
                <label>Precio de referencia:</label>
                <input type="number" value={precioReferencial} onChange={(e) => setPrecioReferencial(e.target.value)} />
            </div>
            <div>
                <label>Precio Venta:</label>
                <input type="number" value={precioVenta} onChange={(e) => setPrecioVenta(e.target.value)} />
            </div>
            <div>
                <label>Año Modelo:</label>
                <input type="number" value={añoModelo} onChange={(e) => setAñoModelo(e.target.value)} />
            </div>
            <button type="submit">Guardar cambios</button>
            <Button onClick={() => history.goBack()}>Cancelar</Button>
        </form>
    );
}

export default EditarDato;