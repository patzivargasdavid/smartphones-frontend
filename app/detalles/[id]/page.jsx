"use client"
import { Box, Card, CardHeader, CardContent, Typography, Grid } from '@mui/material';

async function getUser(id) {
    const apiUrl = `http://localhost:8000/api/smartphones/${id}?timestamp=${Date.now()}`;
    const res = await fetch(apiUrl);
    const data = await res.json()
    return data
}
async function PhoneDetalle({ params }) {
    const pone = await getUser(params.id)
    return (
        <Box mt={2}>
            <Grid container justifyContent="center">
                <Grid item xs={12} sm={8} md={6}>
                    <h1>Detalle con id: {pone.id}</h1>
                    <Card>
                        <CardHeader title={pone.nombre} />
                        <CardContent>
                            <Typography variant="h6" component="h3">
                                {pone.id} {pone.precioReferencial} {pone.last_name}
                            </Typography>
                            <Typography variant="body1" component="p">
                                {pone.precioVenta}
                            </Typography>
                            <Typography variant="body1" component="p">
                                {pone.añoModelo}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </Box>
    )
}


export default PhoneDetalle